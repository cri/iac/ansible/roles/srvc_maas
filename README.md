# srvc-maas

This role aims to deploy a MaaS (v2) worker on a cloud init image.

## Usage

`requirements.yml`:

```yaml
roles:
  - name: forge.srvc_maas
    src: git+https://gitlab.cri.epita.fr/cri/iac/ansible/roles/srvc_maas.git
    version: 0.1.0
```

Example of playbook:

```yaml
---

- hosts: all
  roles:
    - {role: forge.srvc_maas, tags: ['maas']}
```

The deploy services use openstack metadata to get infrormation. An example of
metadata:

```json
{
  "name": "worker-maas-03", // name of the worker

  "gitlab_token": "token gitlab to download maas",
  "version": "version of maas worker",
  "snapshot": "the snapshot version of maas worker",

  "scheduler_url": "scheduler.example.com",
  "tenant_slug": "slug of the tennant",
  "bootstrap_token": "token_bootstrap_tenant",
  "payload_type": "LEGACY_WORKFLOW",

  "nb_cpus": 3, // limit of cpu
  "memory_mb": 1024, // limit of memory

  "registries": [ // list of registries and there access tokens
    {"url": "registry.example.com", "token": "my_token"}
  ],
  "labels": { // list of labels
    "label1": "value1",
    "label2": "value2"
  }
}
```
