#!/usr/bin/env bash

set -euo pipefail
IFS="$(echo -ne ' \t\n')"


OS_API_URL="${OS_API_URL:-http://169.254.169.254}"
OS_API_VERSION="${OS_API_VERSION:-2020-10-14}"
PACKAGE_URL="${PACKAGE_URL:-https://gitlab.cri.epita.fr/api/v4/projects/10471/packages/maven/fr/epita/forge/srvc-maas/}"

BIN_DIR="${BIN_DIR:-/opt/srvc-maas}"
RUN_DIR="${RUN_DIR:-/var/run/srvc-maas/pipelines}"
STATE_DIR="${STATE_DIR:-/var/run/srvc-maas_update}"

log () {
  echo "[$1] $2"
}

logInfo () {
  log "INFO" "$1"
}

logError () {
  log "ERROR" "$1" >&2
}

exitSuccess () {
    logInfo "$1"
    exit 0
}

exitError () {
    logError "$2"
    exit "$1"
}

exitGenericError () {
    exitError 1 "$1"
}

exitArguementError () {
    exitError 2 "$1"
}

extractJson () {
    echo "$1" | jq -re "$2" || exitArguementError "$3"
}

writeFile () {
    FILENAME="$1"
    PERMISSION="$2"

    cat > "$FILENAME" \
        || exitGenericError "Can't write to \"$FILENAME\"."

    chmod "$PERMISSION" "$FILENAME"  \
        || exitGenericError "Can't change \"$FILENAME\" permission"
}

metadataTemplate () {
    extractJson "$METADATA" "$3" "Can't template \"$1\"." | writeFile "$1" "$2"
}

metadataExtract () {
    extractJson "$METADATA" "$1" "can't extract \"$1\" metadata"
}

##################################################
#                  GET METADATA                  #
##################################################

METADATA_STATE_FILE="$STATE_DIR/meta_data.state.json"
METADATA="$(
    curl -f "$OS_API_URL/openstack/$OS_API_VERSION/meta_data.json" \
    | jq -re .meta
)" || exitGenericError 'No metadata'

[ -f "$METADATA_STATE_FILE" ] \
&& echo "$METADATA" | diff "$METADATA_STATE_FILE" - \
    && exitSuccess "No Metadata change"

metadataTemplate "$METADATA_STATE_FILE" 0644 '.'

##################################################
#               DOCKER CONFIG FILE               #
##################################################

DOCKER_CONFIG_FILE="/root/.docker/config.json"

logInfo "Template: \"$DOCKER_CONFIG_FILE\""

metadataTemplate "$DOCKER_CONFIG_FILE" 0644 '.registries | fromjson | {
    auths: (reduce .[] as $i ({}; .[$i.url] = {
        auth: $i.token
    }))
}'

##################################################
#                    MAAS JAR                    #
##################################################

logInfo "Download Maas"

GITLAB_TOKEN="$(metadataExtract .gitlab_token)"
VERSION="$(metadataExtract .version)"
SNAPSHOT="$(metadataExtract .snapshot)"
JAR_FILE="$BIN_DIR/srvc-maas-$VERSION-runner.jar"

curl -f \
    -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
    "$PACKAGE_URL/$VERSION/srvc-maas-$SNAPSHOT-runner.jar" \
    | writeFile "$JAR_FILE" 0644 \
    || exitGenericError "Can't download maas version \"$SNAPSHOT\""

##################################################
#              REGINSTER MAAS WORKER             #
##################################################

logInfo "Register MaaS worker"

SCHEDULER_URL="$(metadataExtract .scheduler_url)"
BOOTSTRAP_TOKEN="$(metadataExtract .bootstrap_token)"
PAYLOAD="$(metadataExtract '{
    name: .name,
    payloadType: .payload_type,
    tenantSlug: .tenant_slug
}')"

REGISTER=$(curl -f -X PUT \
    -H "X-Bootstrap-Token: $BOOTSTRAP_TOKEN" \
    -H "Content-Type: application/json" \
    -d "$PAYLOAD" \
    "${SCHEDULER_URL}api/bootstrap/worker" \
) || exitGenericError "Can't register MaaS worker"

##################################################
#                  EXTRACT TOKEN                 #
##################################################

logInfo "Extract runner token"

TOKEN="$(extractJson "$REGISTER" .token "Can't get worker token")"

##################################################
#            TEMPLATE SYSTEMD SERVICE            #
##################################################

SERVICE_FILE="/etc/systemd/system/srvc-maas.service"

logInfo "Template: \"$SERVICE_FILE\""

NB_CPUS="$(metadataExtract .nb_cpus)"
MEMORY_MB="$(metadataExtract .memory_mb)"
LABELS="$(metadataExtract '.labels | fromjson | [ to_entries[] | join("=")] | join(",")')"

writeFile "$SERVICE_FILE" 0644 <<EOF
[Unit]
Description=MaaS Worker
After=network-online.target

[Service]
Type=simple

WorkingDirectory=$RUN_DIR
ExecStart=java -XX:OnOutOfMemoryError="kill -9 %p" -jar "$JAR_FILE"
Environment=WORKER_SCHEDULER_URL=$SCHEDULER_URL
Environment=WORKER_TOKEN=$TOKEN
Environment=WORKER_NB_CPUS=$NB_CPUS
Environment=WORKER_MEMORY_MB=$MEMORY_MB
Environment=WORKER_LABELS=$LABELS

Restart=on-failure
TimeoutStopSec=300

StandardOutput=syslog
StandardError=syslog
SyslogIdentifier=srvc-maas

[Install]
WantedBy=multi-user.target
EOF

##################################################
#                RESTART SERVICES                #
##################################################

logInfo "Restart docker service"

systemctl restart docker.service \
    || exitGenericError "Can't restart \"docker.service\""

logInfo "Restart srvc-maas service"

systemctl daemon-reload \
    || exitGenericError "Can't reload daemon"

systemctl enable srvc-maas.service \
    || exitGenericError "Can't enable \"srvc-maas.service\""

systemctl restart srvc-maas.service \
    || exitGenericError "Can't restart \"srvc-maas.service\""
